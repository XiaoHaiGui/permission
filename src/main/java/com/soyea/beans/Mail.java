package com.soyea.beans;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Mail {

    //主题
    private String subject;

    //信息内容
    private String message;

    //接收者
    private Set<String> receivers;
}
