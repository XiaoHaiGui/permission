package com.soyea.common;

import java.util.Date;

/**
 * 关于操作的常数类
 * Created by skh on 2018/1/3
 */
public interface OperateConstant {

	String operator = "system-admin";

	Date operateTime = new Date();

	String operateIp = "127.0.0.1";
}
