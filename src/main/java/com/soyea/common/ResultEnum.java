package com.soyea.common;

import lombok.Getter;

/**
 * 返回值枚举类
 * Created by skh on 2017/12/13
 */
@Getter
public enum ResultEnum {
	SUCCESS(0, "成功"),
	PARAM_ERROR(1, "参数不正确"),
	SYSTEM_ERROR(2, "系统出现异常,请联系管理员"),

	EXIST_SAME_DEPTNAME(3, "同一层级下存在相同名称的部门"),
	DEPT_NOT_EXIST(4, "指定部门不存在"),
	ID_IS_NULL(5, "Id为空");



	private Integer code;
	private String message;

	ResultEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public static ResultEnum stateOf(int index) {
		for (ResultEnum state : values()) {
			if (state.getCode() == index) {
				return state;
			}
		}
		return null;
	}
}
