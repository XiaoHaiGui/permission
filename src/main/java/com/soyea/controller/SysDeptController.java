package com.soyea.controller;

import com.google.common.base.Preconditions;
import com.soyea.common.JsonData;
import com.soyea.common.ResultEnum;
import com.soyea.dto.DeptLevelDTO;
import com.soyea.param.DeptParam;
import com.soyea.service.SysDeptService;
import com.soyea.service.SysTreeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 部门
 * Created by skh on 2018/1/3
 */
@RestController
@Slf4j
@RequestMapping("/sys/dept")
public class SysDeptController {

	@Autowired
	private SysDeptService sysDeptService;

	@Autowired
	private SysTreeService sysTreeService;

	/**
	 * 进入部门类别
	 * @return
	 */
	@RequestMapping("/dept.page")
	public ModelAndView page() {
		return new ModelAndView("dept");
	}

	/**
	 * 新增部门
	 * @param deptParam
	 * @return
	 */
	@RequestMapping("/save.json")
	public JsonData saveDept(DeptParam deptParam) {
		sysDeptService.save(deptParam);
		return JsonData.success();
	}

	/**
	 * 获取部门层级树列表
	 * @return
	 */
	@RequestMapping("/tree.json")
	public JsonData tree() {
		List<DeptLevelDTO> dtoList = sysTreeService.deptTree();
		return JsonData.success(dtoList);
	}

	/**
	 * 修改部门
	 * @param deptParam
	 * @return
	 */
	@RequestMapping("/update.json")
	public JsonData updateDept(DeptParam deptParam) {
		Preconditions.checkNotNull(deptParam.getId(), "修改部门:"+ResultEnum.ID_IS_NULL.getMessage());
		sysDeptService.update(deptParam);
		return JsonData.success();
	}

	/**
	 * 删除部门
	 * @param id
	 * @return
	 */
	@RequestMapping("/delete.json")
	@ResponseBody
	public JsonData delete(@RequestParam("id") int id) {
		sysDeptService.delete(id);
		return JsonData.success();
	}



}
