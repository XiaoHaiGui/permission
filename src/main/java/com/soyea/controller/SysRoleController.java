package com.soyea.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.soyea.common.JsonData;
import com.soyea.model.SysUser;
import com.soyea.param.RoleParam;
import com.soyea.service.*;
import com.soyea.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 角色controller
 */
@Controller
@RequestMapping("/sys/role")
public class SysRoleController {

	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysTreeService sysTreeService;
	@Autowired
	private SysRoleAclService sysRoleAclService;
	@Autowired
	private SysRoleUserService sysRoleUserService;
	@Autowired
	private SysUserService sysUserService;

	/**
	 * 进入角色管理页面
	 *
	 * @return
	 */
	@RequestMapping("role.page")
	public ModelAndView page() {
		return new ModelAndView("role");
	}

	/**
	 * 新增角色
	 *
	 * @param param
	 * @return
	 */
	@RequestMapping("/save.json")
	@ResponseBody
	public JsonData saveRole(RoleParam param) {
		sysRoleService.save(param);
		return JsonData.success();
	}

	/**
	 * 更新角色
	 *
	 * @param param
	 * @return
	 */
	@RequestMapping("/update.json")
	@ResponseBody
	public JsonData updateRole(RoleParam param) {
		sysRoleService.update(param);
		return JsonData.success();
	}

	/**
	 * 查询所有角色
	 *
	 * @return
	 */
	@RequestMapping("/list.json")
	@ResponseBody
	public JsonData list() {
		return JsonData.success(sysRoleService.getAll());
	}

	/**
	 * 获取指定角色的树结构的权限列表
	 *
	 * @param roleId
	 * @return
	 */
	@RequestMapping("/roleTree.json")
	@ResponseBody
	public JsonData roleTree(@RequestParam("roleId") int roleId) {
		return JsonData.success(sysTreeService.roleTree(roleId));
	}

	/**
	 * 改变角色和权限的关系 给一个角色分配多个权限
	 *
	 * @param roleId
	 * @param aclIds
	 * @return
	 */
	@RequestMapping("/changeAcls.json")
	@ResponseBody
	public JsonData changeAcls(@RequestParam("roleId") int roleId, @RequestParam(value = "aclIds", required = false, defaultValue = "") String aclIds) {
		List<Integer> aclIdList = StringUtil.splitToListInt(aclIds);
		sysRoleAclService.changeRoleAcls(roleId, aclIdList);
		return JsonData.success();
	}


	/**
	 * 改变角色和用户的关系 给一个角色分配多个用户
	 *
	 * @param roleId
	 * @param userIds
	 * @return
	 */
	@RequestMapping("/changeUsers.json")
	@ResponseBody
	public JsonData changeUsers(@RequestParam("roleId") int roleId, @RequestParam(value = "userIds", required = false, defaultValue = "") String userIds) {
		List<Integer> userIdList = StringUtil.splitToListInt(userIds);
		sysRoleUserService.changeRoleUsers(roleId, userIdList);
		return JsonData.success();
	}


	/**
	 * 查询指定角色下的用户列表,分为选中和未选中
	 *
	 * 思路
	 * 查询指定角色下的用户列表(选中)
	 * 查询出所有用户
	 * 未选中用户=所有用户-已选中用户
	 *
	 *
	 * @param roleId
	 * @return
	 */
	@RequestMapping("/users.json")
	@ResponseBody
	public JsonData users(@RequestParam("roleId") int roleId) {
		//查询指定角色下的用户列表
		List<SysUser> selectedUserList = sysRoleUserService.getListByRoleId(roleId);
		List<SysUser> allUserList = sysUserService.getAll();
		List<SysUser> unselectedUserList = Lists.newArrayList();

		//获取指定角色下的用户列表的用户Id值
		Set<Integer> selectedUserIdSet = selectedUserList.stream().map(sysUser -> sysUser.getId()).collect(Collectors.toSet());

		//获取未选中的用户列表
		for (SysUser sysUser : allUserList) {
			if (sysUser.getStatus() == 1 && !selectedUserIdSet.contains(sysUser.getId())) {
				unselectedUserList.add(sysUser);
			}
		}
		// selectedUserList = selectedUserList.stream().filter(sysUser -> sysUser.getStatus() != 1).collect(Collectors.toList());
		Map<String, List<SysUser>> map = Maps.newHashMap();
		map.put("selected", selectedUserList);
		map.put("unselected", unselectedUserList);
		//返回选中和未选中的用户列表
		return JsonData.success(map);
	}
}
