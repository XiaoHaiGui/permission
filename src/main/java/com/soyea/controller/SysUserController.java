package com.soyea.controller;

import com.google.common.collect.Maps;
import com.soyea.beans.PageQuery;
import com.soyea.beans.PageResult;
import com.soyea.common.JsonData;
import com.soyea.model.SysUser;
import com.soyea.param.UserParam;
import com.soyea.service.SysRoleService;
import com.soyea.service.SysTreeService;
import com.soyea.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 系统用户管理
 * Created by skh on 2018/1/4
 */
@RestController
@Slf4j
@RequestMapping("/sys/user")
public class SysUserController {

	@Autowired
	private SysUserService sysUserService;

	@Resource
	private SysTreeService sysTreeService;
	@Resource
	private SysRoleService sysRoleService;

	@RequestMapping("/noAuth.page")
	public ModelAndView noAuth() {
		return new ModelAndView("noAuth");
	}

	@RequestMapping("/save.json")
	public JsonData saveUser(UserParam param) {
		sysUserService.save(param);
		return JsonData.success();
	}

	@RequestMapping("/update.json")
	public JsonData updateUser(UserParam param) {
		sysUserService.update(param);
		return JsonData.success();
	}


	@RequestMapping("/page.json")
	@ResponseBody
	public JsonData page(@RequestParam("deptId") int deptId, PageQuery pageQuery) {
		PageResult<SysUser> result = sysUserService.getPageByDeptId(deptId, pageQuery);
		return JsonData.success(result);
	}

	/**
	 * 查询指定用户拥有的角色列表和树形权限列表
	 *
	 * 思路:
	 * 调用角色服务,获取角色列表
	 * 调用权限服务,获取权限模块列表(树列表)
	 *
	 * @param userId
	 * @return
	 */
	@RequestMapping("/acls.json")
	@ResponseBody
	public JsonData acls(@RequestParam("userId") int userId) {
		Map<String, Object> map = Maps.newHashMap();
		map.put("acls", sysTreeService.userAclTree(userId));
		map.put("roles", sysRoleService.getRoleListByUserId(userId));
		return JsonData.success(map);
	}


}
