package com.soyea.dao;

import com.soyea.dto.DeptLevelDTO;
import com.soyea.model.SysDept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysDeptMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysDept record);

    int insertSelective(SysDept record);

    SysDept selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysDept record);

    int updateByPrimaryKey(SysDept record);

    /**
     * 获取所有部门
     * @return
     */
    List<SysDept> getAllDept();

    /**
     * 通过level获取所有子部门列表(包括子部门的子部门)
     */
    List<SysDept> getChildDeptListByLevel(@Param("level") String level,@Param("levelLike") String levelLike);


    /**
     * 批量更新部门level
     * @param sysDeptList
     */
    void batchUpdateLevel(@Param("sysDeptList") List<SysDept> sysDeptList);

    /**
     * 查询同一级下的部门名称数量
     * @param parentId
     * @param name
     * @param id
     * @return
     */
    int countByNameAndParentId(@Param("parentId") Integer parentId, @Param("name") String name, @Param("id") Integer id);

    /**
     *
     * @param deptId
     * @return
     */
    int countByParentId(@Param("deptId") int deptId);

    List<SysDept> selectAllDept();

    List<DeptLevelDTO> queryDeptTreeList();

    List<DeptLevelDTO> selectDeptChildrenById(@Param("id")Integer id);
}