package com.soyea.dto;

import com.google.common.collect.Lists;
import com.soyea.model.SysDept;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * 部门层级树
 * Created by skh on 2018/1/3
 */
@Data
public class DeptLevelDTO extends SysDept {
	private List<DeptLevelDTO> deptList = Lists.newArrayList(); //子部门列表

	/**
	 * 拷贝dept属性到dto
	 * @param dept
	 * @return
	 */
	public static DeptLevelDTO adapt(SysDept dept) {
		DeptLevelDTO dto = new DeptLevelDTO();
		BeanUtils.copyProperties(dept, dto);
		return dto;
	}
}
