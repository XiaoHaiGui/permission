package com.soyea.service;

import com.google.common.base.Preconditions;
import com.soyea.common.RequestHolder;
import com.soyea.dao.SysAclMapper;
import com.soyea.dao.SysAclModuleMapper;
import com.soyea.exception.ParamException;
import com.soyea.model.SysAclModule;
import com.soyea.param.AclModuleParam;
import com.soyea.util.BeanValidator;
import com.soyea.util.IpUtil;
import com.soyea.util.LevelUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class SysAclModuleService {

	@Autowired
	private SysAclModuleMapper sysAclModuleMapper;
	@Autowired
	private SysAclMapper sysAclMapper;
    @Autowired
    private SysLogService sysLogService;

	/**
	 * 保存权限模块
	 *
	 * @param param
	 */
	public void save(AclModuleParam param) {
		BeanValidator.check(param);
		if (checkExist(param.getParentId(), param.getName(), param.getId())) {
			throw new ParamException("同一层级下存在相同名称的权限模块");
		}
		SysAclModule aclModule = SysAclModule.builder().name(param.getName()).parentId(param.getParentId()).seq(param.getSeq())
				.status(param.getStatus()).remark(param.getRemark()).build();
		aclModule.setLevel(LevelUtil.calculateLevel(getLevel(param.getParentId()), param.getParentId()));
		aclModule.setOperator(RequestHolder.getCurrentUser().getUsername());
		aclModule.setOperateIp(IpUtil.getUserIP(RequestHolder.getCurrentRequest()));
		aclModule.setOperateTime(new Date());
		sysAclModuleMapper.insertSelective(aclModule);
        sysLogService.saveAclModuleLog(null, aclModule);
	}

	/**
	 * 更新权限模块
	 *
	 * @param param
	 */
	public void update(AclModuleParam param) {
		BeanValidator.check(param);
		if (checkExist(param.getParentId(), param.getName(), param.getId())) {
			throw new ParamException("同一层级下存在相同名称的权限模块");
		}
		SysAclModule before = sysAclModuleMapper.selectByPrimaryKey(param.getId());
		Preconditions.checkNotNull(before, "待更新的权限模块不存在");

		SysAclModule after = SysAclModule.builder().id(param.getId()).name(param.getName()).parentId(param.getParentId()).seq(param.getSeq())
				.status(param.getStatus()).remark(param.getRemark()).build();
		after.setLevel(LevelUtil.calculateLevel(getLevel(param.getParentId()), param.getParentId()));
		after.setOperator(RequestHolder.getCurrentUser().getUsername());
		after.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
		after.setOperateTime(new Date());

		updateWithChild(before, after);
        sysLogService.saveAclModuleLog(before, after);
	}

	/**
	 * 删除权限模块
	 *
	 * @param aclModuleId
	 */
	public void delete(int aclModuleId) {
		SysAclModule aclModule = sysAclModuleMapper.selectByPrimaryKey(aclModuleId);
		Preconditions.checkNotNull(aclModule, "待删除的权限模块不存在，无法删除", aclModuleId);
		if (sysAclModuleMapper.countByParentId(aclModule.getId()) > 0) {
			throw new ParamException("当前模块下面有子模块，无法删除");
		}
        if (sysAclMapper.countByAclModuleId(aclModule.getId()) > 0) {
            throw new ParamException("当前模块下面有权限，无法删除");
        }
		sysAclModuleMapper.deleteByPrimaryKey(aclModuleId);
	}

	private void updateWithChild(SysAclModule before, SysAclModule after) {
		String newLevelPrefix = after.getLevel();
		String oldLevelPrefix = before.getLevel();
		if (!after.getLevel().equals(before.getLevel())) {
			String childLevel = new StringBuilder().append(before.getLevel()).append(".").append(before.getId()).toString();
			String childLevelLike = childLevel.concat(".%");
			//查询出下一等级的子模块
			List<SysAclModule> aclModuleList = sysAclModuleMapper.getChildAclModuleListByLevel(childLevel, childLevelLike);
			if (CollectionUtils.isNotEmpty(aclModuleList)) {
				for (SysAclModule aclModule : aclModuleList) {
					String level = aclModule.getLevel();
					if (level.indexOf(oldLevelPrefix) == 0) {
						level = newLevelPrefix + level.substring(oldLevelPrefix.length());
						aclModule.setLevel(level);
					}
				}
				sysAclModuleMapper.batchUpdateLevel(aclModuleList);
			}
		}
		sysAclModuleMapper.updateByPrimaryKeySelective(after);
	}

	private boolean checkExist(Integer parentId, String aclModuleName, Integer deptId) {
		return sysAclModuleMapper.countByNameAndParentId(parentId, aclModuleName, deptId) > 0;
	}

	private String getLevel(Integer aclModuleId) {
		SysAclModule aclModule = sysAclModuleMapper.selectByPrimaryKey(aclModuleId);
		if (aclModule == null) {
			return null;
		}
		return aclModule.getLevel();
	}


}
