//package com.soyea.service;
//
//import com.google.common.base.Joiner;
//import com.soyea.beans.CacheKeyConstants;
//import com.soyea.util.JsonMapper;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//import redis.clients.jedis.ShardedJedis;
//
//import javax.annotation.Resource;
//
///**
// * 缓存服务
// */
//@Service
//@Slf4j
//public class SysCacheService {
//
//    @Resource(name = "redisPool")
//    private RedisPool redisPool;
//
//    public void saveCache(String toSavedValue, int timeoutSeconds, CacheKeyConstants prefix) {
//        saveCache(toSavedValue, timeoutSeconds, prefix, null);
//    }
//
//	/**
//	 * 缓存
//	 * @param toSavedValue 需要缓存的值
//	 * @param timeoutSeconds 缓存时效
//	 * @param prefix 前缀
//	 * @param keys key的值
//	 */
//    public void saveCache(String toSavedValue, int timeoutSeconds, CacheKeyConstants prefix, String... keys) {
//        if (toSavedValue == null) {
//            return;
//        }
//        ShardedJedis shardedJedis = null;
//        try {
//            String cacheKey = generateCacheKey(prefix, keys);
//            shardedJedis = redisPool.instance();
//            //缓存
//            shardedJedis.setex(cacheKey, timeoutSeconds, toSavedValue);
//        } catch (Exception e) {
//            log.error("save cache exception, prefix:{}, keys:{}", prefix.name(), JsonMapper.obj2String(keys), e);
//        } finally {
//            redisPool.safeClose(shardedJedis);
//        }
//    }
//
//    //从缓存中取出数据
//    public String getFromCache(CacheKeyConstants prefix, String... keys) {
//        ShardedJedis shardedJedis = null;
//        String cacheKey = generateCacheKey(prefix, keys);
//        try {
//            shardedJedis = redisPool.instance();
//            String value = shardedJedis.get(cacheKey);
//            return value;
//        } catch (Exception e) {
//            log.error("get from cache exception, prefix:{}, keys:{}", prefix.name(), JsonMapper.obj2String(keys), e);
//            return null;
//        } finally {
//            redisPool.safeClose(shardedJedis);
//        }
//    }
//
//    //生成缓存key
//    private String generateCacheKey(CacheKeyConstants prefix, String... keys) {
//        String key = prefix.name();
//        if (keys != null && keys.length > 0) {
//            key += "_" + Joiner.on("_").join(keys);
//        }
//        return key;
//    }
//}
