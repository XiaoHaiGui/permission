package com.soyea.service;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.soyea.common.RequestHolder;
import com.soyea.dao.SysRoleAclMapper;
import com.soyea.dao.SysRoleMapper;
import com.soyea.dao.SysRoleUserMapper;
import com.soyea.dao.SysUserMapper;
import com.soyea.exception.ParamException;
import com.soyea.model.SysRole;
import com.soyea.model.SysUser;
import com.soyea.param.RoleParam;
import com.soyea.util.BeanValidator;
import com.soyea.util.IpUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色service
 */
@Service
@Transactional
public class SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private SysRoleUserMapper sysRoleUserMapper;
    @Autowired
    private SysRoleAclMapper sysRoleAclMapper;
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysLogService sysLogService;

    /**
     * 新增角色
     *
     * 思路:类似新增用户
     *
     * @param param
     */
    public void save(RoleParam param) {
        BeanValidator.check(param);
        if (checkExist(param.getName(), param.getId())) {
            throw new ParamException("角色名称已经存在");
        }
        SysRole role = SysRole.builder().name(param.getName()).status(param.getStatus()).type(param.getType())
                .remark(param.getRemark()).build();
        role.setOperator(RequestHolder.getCurrentUser().getUsername());
        role.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        role.setOperateTime(new Date());
        sysRoleMapper.insertSelective(role);
       sysLogService.saveRoleLog(null, role);
    }

    /**
     * 更新角色
     *
     * 思路:类似更新用户
     *
     *
     * @param param
     */
    public void update(RoleParam param) {
        BeanValidator.check(param);
        if (checkExist(param.getName(), param.getId())) {
            throw new ParamException("角色名称已经存在");
        }
        SysRole before = sysRoleMapper.selectByPrimaryKey(param.getId());
        Preconditions.checkNotNull(before, "待更新的角色不存在");

        SysRole after = SysRole.builder().id(param.getId()).name(param.getName()).status(param.getStatus()).type(param.getType())
                .remark(param.getRemark()).build();
        after.setOperator(RequestHolder.getCurrentUser().getUsername());
        after.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        after.setOperateTime(new Date());
        sysRoleMapper.updateByPrimaryKeySelective(after);
        sysLogService.saveRoleLog(before, after);
    }

    /**
     * 获取所有角色
     * @return
     */
    public List<SysRole> getAll() {
        return sysRoleMapper.getAll();
    }

    /**
     * 查询指定用户的角色列表
     *
     * 思路:
     * 通过role_user表查询出指定用户Id对应的roleIdList
     * select roleId from role_user where userId=#{userId};
     * 根据roleIdList查询出角色列表
     * select * from role where roleId in (roleIdList)
     *
     * @param userId
     * @return
     */
    public List<SysRole> getRoleListByUserId(int userId) {
        //先通过role_user表查询出指定用户Id对应的roleIdList
        List<Integer> roleIdList = sysRoleUserMapper.getRoleIdListByUserId(userId);
        if (CollectionUtils.isEmpty(roleIdList)) {
            return Lists.newArrayList();
        }
        //再根据roleIdList查询出角色列表
        return sysRoleMapper.getByIdList(roleIdList);
    }

    /**
     * 查询指定权限属于哪些角色列表
     *
     * 思路
     * aclId->roleIdList
     * roleIdList->roleList
     *
     * @param aclId
     * @return
     */
    public List<SysRole> getRoleListByAclId(int aclId) {
        List<Integer> roleIdList = sysRoleAclMapper.getRoleIdListByAclId(aclId);
        if (CollectionUtils.isEmpty(roleIdList)) {
            return Lists.newArrayList();
        }
        return sysRoleMapper.getByIdList(roleIdList);
    }

    /**
     * 通过角色列表查询出用户列表
     *
     * 思路
     * 根据角色列表获取角色id列表
     * 根据角色id列表查询出用户id列表
     * 根据用户id列表查询出用户列表
     *
     * @param roleList
     * @return
     */
    public List<SysUser> getUserListByRoleList(List<SysRole> roleList) {
        if (CollectionUtils.isEmpty(roleList)) {
            return Lists.newArrayList();
        }

        //先通过所有roleIdList查询出userIdList,再使用userMapper查询出所有用户
        List<Integer> roleIdList = roleList.stream().map(role -> role.getId()).collect(Collectors.toList());
        List<Integer> userIdList = sysRoleUserMapper.getUserIdListByRoleIdList(roleIdList);
        if (CollectionUtils.isEmpty(userIdList)) {
            return Lists.newArrayList();
        }
        return sysUserMapper.getByIdList(userIdList);
    }

    private boolean checkExist(String name, Integer id) {
        return sysRoleMapper.countByName(name, id) > 0;
    }

}
