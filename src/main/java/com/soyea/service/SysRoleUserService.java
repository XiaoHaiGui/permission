package com.soyea.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import com.soyea.beans.LogType;
import com.soyea.common.RequestHolder;
import com.soyea.dao.SysLogMapper;
import com.soyea.dao.SysRoleUserMapper;
import com.soyea.dao.SysUserMapper;
import com.soyea.model.SysLogWithBLOBs;
import com.soyea.model.SysRoleUser;
import com.soyea.model.SysUser;
import com.soyea.util.IpUtil;
import com.soyea.util.JsonMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 角色-用户 service
 */
@Service
@Transactional
public class SysRoleUserService {

    @Resource
    private SysRoleUserMapper sysRoleUserMapper;
    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private SysLogMapper sysLogMapper;

    /**
     * 查询指定角色下的用户列表
     * @param roleId
     * @return
     */
    public List<SysUser> getListByRoleId(int roleId) {
        List<Integer> userIdList = sysRoleUserMapper.getUserIdListByRoleId(roleId);
        if (CollectionUtils.isEmpty(userIdList)) {
            return Lists.newArrayList();
        }
        return sysUserMapper.getByIdList(userIdList);
    }

    /**
     * 改变角色和用户的关系 给一个角色分配多个用户
     * @param roleId
     * @param userIdList
     */
    public void changeRoleUsers(int roleId, List<Integer> userIdList) {
        //先查询出更新前的用户IdList
        List<Integer> originUserIdList = sysRoleUserMapper.getUserIdListByRoleId(roleId);

        //比如说 超级管理员 3人  更新后还是3人 需要判断是否是原来的3人. 如果是,则不用更新.
        if (originUserIdList.size() == userIdList.size()) {
            //通过set的方式来去重
            Set<Integer> originUserIdSet = Sets.newHashSet(originUserIdList);
            Set<Integer> userIdSet = Sets.newHashSet(userIdList);
            originUserIdSet.removeAll(userIdSet);
            if (CollectionUtils.isEmpty(originUserIdSet)) {
                //说明更新后还是原来的人,则不需要更新
                return;
            }
        }
        //更新关系
        updateRoleUsers(roleId, userIdList);
        saveRoleUserLog(roleId, originUserIdList, userIdList);
    }

    private void updateRoleUsers(int roleId, List<Integer> userIdList) {
        //先删除指定角色Id的所有用户
        sysRoleUserMapper.deleteByRoleId(roleId);

        if (CollectionUtils.isEmpty(userIdList)) {
            //说明所有用户都没有角色
            return;
        }

        List<SysRoleUser> roleUserList = Lists.newArrayList();
        for (Integer userId : userIdList) {
            SysRoleUser roleUser = SysRoleUser.builder().roleId(roleId).userId(userId).operator(RequestHolder.getCurrentUser().getUsername())
                    .operateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest())).operateTime(new Date()).build();
            roleUserList.add(roleUser);
        }
        //再批量新增指定角色id的用户列表
        sysRoleUserMapper.batchInsert(roleUserList);
    }
    private void saveRoleUserLog(int roleId, List<Integer> before, List<Integer> after) {
        SysLogWithBLOBs sysLog = new SysLogWithBLOBs();
        sysLog.setType(LogType.TYPE_ROLE_USER);
        sysLog.setTargetId(roleId);
        sysLog.setOldValue(before == null ? "" : JsonMapper.obj2String(before));
        sysLog.setNewValue(after == null ? "" : JsonMapper.obj2String(after));
        sysLog.setOperator(RequestHolder.getCurrentUser().getUsername());
        sysLog.setOperateIp(IpUtil.getRemoteIp(RequestHolder.getCurrentRequest()));
        sysLog.setOperateTime(new Date());
        sysLog.setStatus(1);
        sysLogMapper.insertSelective(sysLog);
    }
}
