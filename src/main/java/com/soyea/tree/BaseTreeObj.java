package com.soyea.tree;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 基层树形结构实体类
 * 必备属性:id,parentId,childsList
 */
@Data
public class BaseTreeObj implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String parentId;
	private String name;
	private List<BaseTreeObj> childsList = new ArrayList<>();
}