package com.soyea.util;

import com.google.common.base.Splitter;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.List;
import java.util.stream.Collectors;

public class StringUtil {


	/**
	 * 将字符串根据逗号分割,生成List<String>
	 *
	 * @param str
	 * @return
	 */
	public static List<String> splitToListStr(String str) {
		List<String> strList = Splitter.on(",").trimResults().omitEmptyStrings().splitToList(str);
		return strList;
	}

	/**
	 * 将字符串根据逗号分割,生成List<Integer>
	 *
	 * @param str
	 * @return
	 */
	public static List<Integer> splitToListInt(String str) {
		List<String> strList = Splitter.on(",").trimResults().omitEmptyStrings().splitToList(str);
		return strList.stream().map(strItem -> Integer.parseInt(strItem)).collect(Collectors.toList());
	}

	/**
	 * MD5加密
	 *
	 * @param str
	 * @return
	 */
	public static String md5Encrypt(String str) {
		return DigestUtils.md5Hex(str);
	}


	public static void main(String[] args) {
		String oldLevel = "0,1,";
		String oldLevel1 = "0,1,3,";
		String oldLevel2 = "0,1,3,4,";

		String newLevel ="0,2,9,";
		String newLevel1 ="0,2,9,3,";
		String newLevel2 ="0,2,9,3,4,";

		String result1 = newLevel + oldLevel1.substring(oldLevel.length());
		System.out.println("result1 = " + result1);

		String result2 = newLevel + oldLevel2.substring(oldLevel.length());
		System.out.println("result2 = " + result2);

	}

}
