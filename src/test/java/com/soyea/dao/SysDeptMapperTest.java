package com.soyea.dao;

import com.google.common.collect.Lists;
import com.soyea.BaseTest;
import com.soyea.model.SysDept;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by skh on 2018/1/3
 */

@Slf4j
@Transactional
public class SysDeptMapperTest extends BaseTest{


	@Autowired
	private SysDeptMapper sysDeptMapper;

	@Test
	public void getAllDept() throws Exception {
		List<SysDept> deptList = sysDeptMapper.getAllDept();
		log.debug("getAllDept:{}",deptList);
		log.debug("size:{}",deptList.size());
		Assert.assertNotEquals(0,deptList.size());
	}

	@Test
	public void getChildDeptListByLevel() throws Exception {
		String childLevel = new StringBuilder().append(0).append(".").append(12).toString();
		String childLevelLike = childLevel.concat(".%");
		List<SysDept> deptList = sysDeptMapper.getChildDeptListByLevel(childLevel,childLevelLike);
		log.debug("getChildDeptListByLevel:{}",deptList);
		Assert.assertNotEquals(0,deptList.size());
	}

	@Test
	public void batchUpdateLevel() throws Exception {
		List<SysDept> sysDeptList = Lists.newArrayList();
		SysDept sysDept1 = sysDeptMapper.selectByPrimaryKey(12);
		sysDept1.setLevel("0.11");
		SysDept sysDept2 = sysDeptMapper.selectByPrimaryKey(13);
		sysDept2.setLevel("0.11");
		sysDeptList.add(sysDept1);
		sysDeptList.add(sysDept2);

		sysDeptMapper.batchUpdateLevel(sysDeptList);
		assertEquals("0.11",sysDept1.getLevel());
		assertEquals("0.11",sysDept2.getLevel());
	}

	@Test
	public void countByNameAndParentId() throws Exception {
		Integer parentId = 0;
		String name = "技术部";
		Integer id = 1;
		int count = sysDeptMapper.countByNameAndParentId(parentId, name,1);
		log.debug("count:{}",count);
		assertEquals(0,count);
	}

}