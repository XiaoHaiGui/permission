package com.soyea.service;

import com.soyea.BaseTest;
import com.soyea.model.SysDept;
import com.soyea.param.DeptParam;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

/**
 * Created by skh on 2018/1/4
 */
@Slf4j
public class SysDeptServiceTest extends BaseTest {


	@Autowired
	private SysDeptService sysDeptService;

	@Test
	public void save() throws Exception {
		DeptParam deptParam = new DeptParam();
		deptParam.setName("后勤部");
		deptParam.setParentId(0);
		deptParam.setRemark("打扫卫生");
		deptParam.setSeq(0);
		sysDeptService.save(deptParam);
		log.debug("id:{}", deptParam.getId());
	}

	@Test
	public void update() throws Exception {
		DeptParam deptParam = new DeptParam();
		deptParam.setId(16);
		deptParam.setName("后勤部修改");
		deptParam.setParentId(12);
		deptParam.setRemark("打扫卫生");
		deptParam.setSeq(0);
		sysDeptService.update(deptParam);
		log.debug("id:{}", deptParam.getId());
	}

	@Test
	public void showDetail() throws Exception {
		Integer id = 1;
		SysDept sysDept = sysDeptService.showDetail(id);
		log.debug("dept = {}", sysDept);
		assertNotEquals(null,sysDept);
	}

	@Test
	public void checkExist() throws Exception {
		Integer parentId = 0;
		String name = "技术部";
		Integer id = 1;
		boolean exist = sysDeptService.checkExist(parentId, name, null);
		log.debug("exist:{}", exist);
		assertEquals(true, exist);

		boolean exist1 = sysDeptService.checkExist(parentId, name, 1);
		log.debug("exist1:{}", exist1);
		assertEquals(false, exist1);
	}

	@Test
	public void getParentLevel() throws Exception {
		Integer parentId = 2;
		String parentLevel = sysDeptService.getParentLevel(parentId);
		log.debug("parentLevel = {}", parentLevel);
		assertEquals("0.1", parentLevel);
	}

	@Test
	public void updateWithChild() throws Exception {
		Integer id = 1;
		SysDept before = sysDeptService.showDetail(1);
		//parentId不变

		SysDept after = new SysDept();
		BeanUtils.copyProperties(before, after);
		after.setLevel("0.11");
		//0.11 0.11.1 0.11.1 0.11.1 0.11.1.2
		sysDeptService.updateWithChild(before,after);

	}

}