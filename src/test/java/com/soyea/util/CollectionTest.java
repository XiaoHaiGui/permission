package com.soyea.util;

import com.google.common.base.Function;
import com.google.common.collect.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.Test;

import java.util.*;

/**
 * guava 集合和函数接口妙用
 * Created by skh on 2018/1/15
 */
public class CollectionTest {

	//partition:把iterable按指定大小分割，得到的子集都不能进行修改操作
	//利用partition进行对数据进行分组,根据size的大小进行分组
	@Test
	public void test26() {
		List<String> list = ImmutableList.of("hello", "HI", "Hey");
		List<List<String>> partition = Lists.partition(list, 2);
		System.out.println(partition);
	}

	@Test
	public void test1() {
		List<String> list = Lists.newArrayList("hello", "wolrd", "hi", "world", "wahaha");
		List<List<String>> partition = Lists.partition(list, 2);
		System.out.println("partition = " + partition);
	}

	//Lists中的transform方法（通过函数式接口）能够对数据进行处理
	@Test
	public void test25() {
		List<String> list = ImmutableList.of("hello", "HI", "Hey");
		List<String> transform = Lists.transform(list, new Function<String, String>() {
			/**
			 * input为List中的每个数据
			 * @param input
			 * @return 返回处理后的数据
			 */
			@Override
			public String apply(String input) {
				if (input.contentEquals("HI"))
					System.out.println(input);
				return input;
			}
		});
		System.out.println(transform);
	}

	@Test
	public void test2() {
		List<String> list = Lists.newArrayList("hello", "wolrd", "hi", "world", "wahaha");
		List<Object> result = Lists.transform(list, new Function<String, Object>() {

			@Override
			public Object apply(String s) {
				System.out.println("s = " + s);

				if (s.equals("hello")) {
					s = s.toUpperCase();
				}

				return s;
			}
		});

		System.out.println("result = " + result);

	}

	//通过函数式接口，把处理后的数据放到一个map中
	// 根据特征进行筛选集合中的数据
	@Test
	public void test12() {
		ImmutableSet<String> digits = ImmutableSet.of("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine");

		Function<String, Integer> lengthFunction = new Function<String, Integer>() {
			public Integer apply(String string) {
				System.out.println("string = " + string);
				return string.length();
			}
		};

		/**
		 Multimaps.index 参数说明:

		 Iterable<V> values,  需要处理的集合
		 Function<? super V, K> keyFunction 处理函数

		 返回值
		 key -> keyFunction的返回值
		 value -> List<>

		 注意: 该方法返回的是不可变的map,不能再对map中的元素进行操作.
		 */
		ImmutableListMultimap<Integer, String> digitsByLength = Multimaps.index(digits, lengthFunction);
		System.out.println(digitsByLength);
	}

	//List转MultiMap
	/*
	这个是最常见的一种数据结构的转换，常见的场景如下：
	1 计算每个班级所有学生的平均分(将学生按照班级聚合成MultiMap)
	2 计算每个城市酒店的平均产单量(将酒店按照城市聚合成MultiMap)
	 */
	@Test
	public void list2MultimapConvert() {
		//原始方法
		List<Student> studentList = Lists.newArrayList();
		Student student1 = new Student("001", "Mike");
		Student student2 = new Student("001", "Amy");
		Student student3 = new Student("002", "Bob");
		Student student4 = new Student("002", "Flower");
		Student student5 = new Student("003", "Cindy");
		Student student6 = new Student("004", "Wahaha");
		studentList.add(student1);
		studentList.add(student2);
		studentList.add(student3);
		studentList.add(student4);
		studentList.add(student5);
		studentList.add(student6);

		Map<String, List<Student>> map = new HashMap<>();
		for (Student student : studentList) {
			if (!map.containsKey(student.getClassNo())) {
				//新建map
				map.put(student.getClassNo(), Lists.newArrayList());
			}
			//添加元素
			map.get(student.getClassNo()).add(student);
		}
		System.out.println("map = " + map);
	}

	//List转MultiMap
	@Test
	public void list2MultimapConvertByGuava() {
		//Guava方法
		List<Student> studentList = Lists.newArrayList();
		Student student1 = new Student("001", "Mike");
		Student student2 = new Student("001", "Amy");
		Student student3 = new Student("002", "Bob");
		Student student4 = new Student("002", "Flower");
		Student student5 = new Student("003", "Cindy");
		Student student6 = new Student("004", "Wahaha");
		studentList.add(student1);
		studentList.add(student2);
		studentList.add(student3);
		studentList.add(student4);
		studentList.add(student5);
		studentList.add(student6);

		//该方法返回不可变的multimap
		Multimap<String, Student> multimap = Multimaps.index(studentList, new Function<Student, String>() {
			@Override
			public String apply(Student student) {
				return student.getClassNo();
			}
		});
		System.out.println("multimap = " + multimap);


		//该方法需要循环,但是返回的multimap可变
		ArrayListMultimap<String, Student> listMultimap = ArrayListMultimap.create();
		for (Student student : studentList) {
			listMultimap.put(student.getClassNo(), student);
		}
		System.out.println("listMultimap = " + listMultimap);

		//multimap遍历
		for (Map.Entry<String, Collection<Student>> entry : listMultimap.asMap().entrySet()) {
			String key = entry.getKey();
			System.out.println("key = " + key);
			Collection<Student> value = entry.getValue();
			System.out.println("value = " + value);
		}

		//MultiMap转List
		/*
		MultiMap转换成List有几种情况：

		将所有的Key转换成List
		将所有的Value集合转换成Map
		 */

		Set<String> keySet = listMultimap.keySet();
		System.out.println("keySet = " + keySet);
		Collection<Student> values = listMultimap.values();
		System.out.println("values = " + values);


	}

	@Test
	public void list2Map() {
		List<Student> studentList = Lists.newArrayList();
		Student student1 = new Student("001", "Mike");
//		Student student2 = new Student("001", "Amy");
//		Student student3 = new Student("002", "Bob");
		Student student4 = new Student("002", "Flower");
		Student student5 = new Student("003", "Cindy");
		Student student6 = new Student("004", "Wahaha");
		studentList.add(student1);
//		studentList.add(student2);
//		studentList.add(student3);
		studentList.add(student4);
		studentList.add(student5);
		studentList.add(student6);

		//普通方法
		Map<String, Student> map = Maps.newHashMap();
		for (Student student : studentList) {
			map.put(student.getClassNo(), student);
		}
		System.out.println("map = " + map);

		//guava方法
		ImmutableMap<String, Student> immutableMap = Maps.uniqueIndex(studentList, new Function<Student, String>() {
			@Override
			public String apply(Student student) {
				return student.getClassNo();
			}
		});
		System.out.println("immutableMap = " + immutableMap);
	}

	@Data
	@AllArgsConstructor
	class Student {
		private String classNo;
		private String name;
	}

	//multiSet可存放重复元素,可用来统计重复元素.
	@Test
	public void multiSetTest() {
		ArrayList<String> list = Lists.newArrayList("RED", "GREEN", "RED", "WWW");
		HashMultiset<String> multiSet = HashMultiset.create();
		multiSet.addAll(list);
		//count word “the”
		System.out.println("multiSet = " + multiSet);
		Integer count = multiSet.count("RED");
		System.out.println("count = " + count);

		 multiSet.setCount("wahaha", 3);
		System.out.println("multiSet = " + multiSet);
		int size = multiSet.size();
		System.out.println("size = " + size);
	}
}
